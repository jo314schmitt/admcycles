.. _grrcomp_module:

Module ``GRRcomp``
==================

.. automodule:: admcycles.GRRcomp
   :members:
   :undoc-members:
   :show-inheritance:
