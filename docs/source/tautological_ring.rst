.. _tautological_ring:

Module ``tautological ring``
============================

.. automodule:: admcycles.tautological_ring
  :members:
  :undoc-members:
  :show-inheritance:
