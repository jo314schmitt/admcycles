.. _DRmodule:

Module ``DR`` (by Aaron Pixton)
===============================

.. automodule:: admcycles.DR
   :members:
   :undoc-members:
   :show-inheritance:

Generators of the strata algebra
--------------------------------

.. automodule:: admcycles.DR.graph
   :members:
   :undoc-members:
   :show-inheritance:

Moduli types
------------

.. automodule:: admcycles.DR.moduli
   :members:
   :undoc-members:
   :show-inheritance:

Evaluation
----------

.. automodule:: admcycles.DR.evaluation
   :members:
   :undoc-members:
   :show-inheritance:

Multiplication in the strata algebra
------------------------------------

.. automodule:: admcycles.DR.algebra
   :members:
   :undoc-members:
   :show-inheritance:

Relations (Faber-Zagier and pairing)
------------------------------------

.. automodule:: admcycles.DR.relations
   :members:
   :undoc-members:
   :show-inheritance:

Double ramification cycle
-------------------------

.. automodule:: admcycles.DR.double_ramification_cycle
   :members:
   :undoc-members:
   :show-inheritance:

Tools
-----

.. automodule:: admcycles.DR.utils
   :members:
   :undoc-members:
   :show-inheritance:
