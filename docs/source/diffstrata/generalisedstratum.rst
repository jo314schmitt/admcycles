.. _generalisedstratummodule:

Module ``diffstrata.generalisedstratum``
========================================

.. automodule:: admcycles.diffstrata.generalisedstratum
   :members:
   :undoc-members:
   :show-inheritance:
