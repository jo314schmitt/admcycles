.. _double_ramification_cycle_module:

Module ``double_ramification_cycle``
=====================================

.. automodule:: admcycles.double_ramification_cycle
   :members:
   :undoc-members:
   :show-inheritance:
