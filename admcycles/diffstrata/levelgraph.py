from copy import deepcopy
from collections import defaultdict
import itertools

# pylint does not know sage
from sage.modules.free_module import FreeModule  # pylint: disable=import-error
from sage.rings.integer_ring import ZZ  # pylint: disable=import-error
from sage.arith.functions import lcm  # pylint: disable=import-error
from sage.misc.flatten import flatten  # pylint: disable=import-error
from sage.structure.sage_object import SageObject  # pylint: disable=import-error
from sage.misc.mrange import cartesian_product_iterator  # pylint: disable=import-error
from sage.arith.misc import gcd  # pylint: disable=import-error
from sage.functions.generalized import sign
from sage.graphs.graph import Graph  # pylint: disable=import-error
from sage.plot.text import text  # pylint: disable=import-error

from admcycles.stable_graph import StableGraph as stgraph
import admcycles.diffstrata
from admcycles.diffstrata.sig import Signature


def smooth_LG(sig, cover_of_k=None, deck=None):
    r"""
    The smooth (i.e. one vertex) LevelGraph in the stratum (sig).

    INPUT:

    sig (Signature): signature of the stratum.

    OUTPUT:

    LevelGraph: The smooth graph in the stratum.
    """
    return LevelGraph.fromPOlist(
        [sig.g], [list(range(1, sig.n + 1))], [], sig.sig, [0], sig.k, cover_of_k, deck)


class LevelGraph(SageObject):
    r"""
    Create a (stable) level graph.

    ..NOTE::

        This is a low-level class and should NEVER be invoced directly!
        Preferably, EmbeddedLevelGraphs should be used and these should be
        generated automatically by Stratum (or GeneralisedStratum).

    .. NOTE::

        We don't inherit from stgraph/StableGraph anymore, as LevelGraphs
        should be static objects!

    Extends admcycles stgraph to represent a level graph as a stgraph,
    i.e. a list of genera, a list of legs and a list of edges, plus a list of
    poleorders for each leg and a list of levels for each vertex.

    INPUT:

    genera : list
    List of genera of the vertices of length m.

    legs : list
    List of length m, where ith entry is list of legs attached to vertex i.
    By convention, legs are unique positive integers.

    edges : list
    List of edges of the graph. Each edge is a 2-tuple of legs.

    poleorders : dictionary
    Dictionary of the form leg number : poleorder

    levels : list
    List of length m, where the ith entry corresponds to the level of
    vertex i.
    By convention, top level is 0 and levels go down by 1, in particular
    they are NEGATIVE numbers!

    ALTERNATIVELY, the pole orders can be supplied as a list by calling
    LevelGraph.fromPOlist:

    poleorders : list
    List of order of the zero (+) or pole (-) at each leg. The ith element
    of the list corresponds to the order at the leg with the marking i+1
    (because lists start at 0 and the legs are positive integers).

    EXAMPLES:

    Creating a level graph with three components on different levels of genus 1,
    3 and 0. The bottom level has a horizontal node.::

        sage: from admcycles.diffstrata import *
        sage: G = LevelGraph.fromPOlist([1,3,0],[[1,2],[3,4,5],[6,7,8,9]],[(2,3),(5,6),(7,8)],[2,-2,0,6,-2,0,-1,-1,0],[-2,-1,0]); G  #  doctest: +SKIP
        LevelGraph([1, 3, 0],[[1, 2], [3, 4, 5], [6, 7, 8, 9]],[(3, 2), (6, 5), (7, 8)],{1: 2, 2: -2, 3: 0, 4: 6, 5: -2, 6: 0, 7: -1, 8: -1, 9: 0},[-2, -1, 0])

    or alternatively::

        sage: LevelGraph([1, 3, 0],[[1, 2], [3, 4, 5], [6, 7, 8, 9]],[(3, 2), (6, 5), (7, 8)],{1: 2, 2: -2, 3: 0, 4: 6, 5: -2, 6: 0, 7: -1, 8: -1, 9: 0},[-2, -1, 0])
        LevelGraph([1, 3, 0],[[1, 2], [3, 4, 5], [6, 7, 8, 9]],[(3, 2), (6, 5), (7, 8)],{1: 2, 2: -2, 3: 0, 4: 6, 5: -2, 6: 0, 7: -1, 8: -1, 9: 0},[-2, -1, 0])

    We get a warning if the graph has non-stable components: (not any more ;-))::

        sage: G = LevelGraph.fromPOlist([1,3,0,0],[[1,2],[3,4,5],[6,7,8,9],[10]],[(2,3),(5,6),(7,8),(9,10)],[2,-2,0,6,-2,0,-1,-1,0,-2],[-3,-2,0,-1]); G  # doctest: +SKIP
        Warning: Component 3 is not stable: g = 0 but only 1 leg(s)!
        Warning: Graph not stable!
        LevelGraph([1, 3, 0, 0],[[1, 2], [3, 4, 5], [6, 7, 8, 9], [10]],[(3, 2), (6, 5), (7, 8), (9, 10)],{1: 2, 2: -2, 3: 0, 4: 6, 5: -2, 6: 0, 7: -1, 8: -1, 9: 0, 10: -2},[-3, -2, 0, -1])
    """

    def __init__(self, genera, legs, edges, poleorders, levels, k=1, cover_of_k=None, deck=None):
        if len(genera) != len(legs):
            raise ValueError('genera and legs must have the same length')
        self.genera = genera
        self.legs = legs
        self.poleorders = poleorders
        self.levels = levels
        self.edges = []
        for e0, e1 in edges:
            # the edges must be of the form (e0, e1) where e0 is on higher
            # level than e1.
            if self.levelofleg(e0) >= self.levelofleg(e1):
                self.edges.append((e0, e1))
            else:
                self.edges.append((e1, e0))
        self.k = k
        # the signature consists of the marked points that are not half-edges
        sig_list = tuple(poleorders[l] for l in self.list_markings())
        self.sig = Signature(sig_list, k)
        # associated stgraph...
        self._stgraph = None
        self._init_more()  # initiate some things and make sure all is in order
        self._has_long_edge = None
        self._is_long = {}
        self._is_bic = None

        if cover_of_k is None:
            self.cover_of_k = k
        else:
            self.cover_of_k = cover_of_k
        if deck is None:
            if self.cover_of_k != k:
                raise ValueError("If cover_of_k != k you need to provide a deck transformation")
            self.deck = {l: l for l in itertools.chain(*self.legs)}
        elif deck == "UNINIT":
            self.deck = None
        else:
            self.deck = deck
            if not all((deck[e[0]], deck[e[1]]) in edges for e in edges):
                raise ValueError("The deck transformation is not valid.")

    @classmethod
    def fromPOlist(cls, genera, legs, edges,
                   poleordersaslist, levels, k=1, cover_of_k=None, deck=None):
        r"""
        This gives a LevelGraph where the poleorders are given as a list, not
        directly as a dictionary.
        """
        # translate poleorder list to dictionary:
        sortedlegs = sorted(itertools.chain(*legs))
        if len(sortedlegs) != len(poleordersaslist):
            raise ValueError("Numbers of legs and pole orders do not agree!" +
                             " Legs: " + repr(len(sortedlegs)) +
                             " Pole orders: " + repr(len(poleordersaslist)))
        poleorderdict = {sortedlegs[i]: poleordersaslist[i]
                         for i in range(len(sortedlegs))}
        return cls(genera, legs, edges, poleorderdict, levels, k, cover_of_k,
                   deck)

    def __repr__(self):
        return "LevelGraph(" + self.input_as_string() + ")"

    def __hash__(self):
        return hash(repr(self))

    def __str__(self):
        if self.k == 1:
            return ("LevelGraph " + repr(self.genera) + ' ' + repr(self.legs) + ' '
                    + repr(self.edges) + ' ' + repr(self.poleorders) + ' '
                    + repr(self.levels))
        else:
            return ("LevelGraph " + repr(self.genera) + ' ' + repr(self.legs) + ' '
                    + repr(self.edges) + ' ' + repr(self.poleorders) + ' '
                    + repr(self.levels)) + ' ' + repr(self.k) + ' ' + repr(self.cover_of_k) + ' ' + repr(self.deck)

    def input_as_string(self):
        r"""
        return a string that can be given as argument to __init__ to give self
        """
        if self.k == 1:
            return (repr(self.genera) + ',' + repr(self.legs) + ','
                    + repr(self.edges) + ',' + repr(self.poleorders) + ','
                    + repr(self.levels))
        else:
            return (repr(self.genera) + ',' + repr(self.legs) + ','
                    + repr(self.edges) + ',' + repr(self.poleorders) + ','
                    + repr(self.levels) + ',' + repr(self.k) + ','
                    + repr(self.cover_of_k) + ',' + repr(self.deck))

    def __eq__(self, other):
        if not isinstance(other, LevelGraph):
            return False
        return (
            self.genera == other.genera and
            self.legs == other.legs and
            set(self.edges) == set(other.edges) and
            self.poleorders == other.poleorders and
            self.levels == other.levels and
            self.k == other.k and
            self.cover_of_k == other.cover_of_k and
            self.deck == other.deck)

    def g(self):
        genus = sum(self.genera) + len(self.edges) - \
            len(self.genera) + ZZ.one()
        assert genus == self.sig.g, "Signature %r does not match %r!" % (
            self.sig, self)
        return genus

    @property
    def stgraph(self):
        if self._stgraph is None:
            self._stgraph = stgraph([int(g) for g in self.genera],
                                    [[int(l) for l in leg_list]
                                     for leg_list in self.legs],
                                    self.edges,
                                    check=False)
        return self._stgraph

    def list_markings(self, v=None):
        r"""
        Return the list of markings (non-edge legs) of self at vertex v.
        """
        if v is None:
            return tuple(j for v in range(len(self.genera))
                         for j in self.list_markings(v))
        s = set(self.legs[v])
        for e in self.edges:
            s -= set(e)
        return tuple(s)

    @property
    def prongs(self):
        r"""
        Generate the dictionary edge : prong order.
        The prong order is the pole order of the higher-level pole +1
        """
        return {e: self.prong(e) for e in self.edges}

    def vertex(self, leg):
        r"""
        The vertex (as index of genera) where leg is located.

        Args:
            leg (int): leg

        Returns:
            int: index of genera
        """
        return self.legdict[leg]

    def levelofvertex(self, v):
        return self.levels[v]

    def levelofleg(self, leg):
        return self.levelofvertex(self.vertex(leg))

    def orderatleg(self, leg):
        return self.poleorders[leg]

    def ordersonvertex(self, v):
        return [self.orderatleg(leg) for leg in self.legsatvertex(v)]

    def verticesonlevel(self, level):
        return [v for v in range(len(self.genera))
                if self.levelofvertex(v) == level]

    def edgesatvertex(self, v):
        return [e for e in self.edges if self.vertex(e[0]) == v or
                self.vertex(e[1]) == v]

    def legsatvertex(self, v):
        return self.legs[v]

    def is_halfedge(self, leg):
        r"""
        Check if leg has an edge attached to it.
        """
        return any(e[0] == leg or e[1] == leg for e in self.edges)

    def genus(self, v=None):
        r"""
        Return the genus of vertex v.

        If v is None, return genus of the complete LevelGraph.
        """
        if v is None:
            return self.g()  # from admcycles
        else:
            return self.genera[v]

    def numberoflevels(self):
        return len(set(self.levels))

    def is_bic(self):
        if self._is_bic is None:
            self._is_bic = self.numberoflevels() == 2 and not self.is_horizontal()
        return self._is_bic

    def codim(self):
        r"""
        Return the codim = No. of levels of self + horizontal edges.
        """
        return (self.numberoflevels() - 1 +
                sum(1 for e in self.edges if self.is_horizontal(e)))

    def edgesatlevel(self, level):
        r"""
        Return list of edges with at least one node at level.
        """
        return [e for e in self.edges if self.levelofleg(e[0]) == level or
                self.levelofleg(e[1]) == level]

    def horizontaledgesatlevel(self, level):
        return [e for e in self.edgesatlevel(level) if self.is_horizontal(e)]

    def nextlowerlevel(self, l):
        r"""
        Return the next lower level number or  False if no legal level
        (e.g. lowest level) is given as input.

        Point of discussion: Is it better to tidy up the level numbers
        to be directly ascending or not?

        Pro tidy: * this becomes obsolete ;)
                  * easier to check isomorphisms?
        Con tidy: * we "see" where we came from
                  * easier to undo/glue back in after cutting
        """
        try:
            llindex = self.sortedlevels.index(l) - 1
        except ValueError:
            return False
        if llindex == -1:
            return False
        else:
            return self.sortedlevels[llindex]

    def internal_level_number(self, i):
        r"""
        Return the internal i-th level, e.g.

        self.levels = [0,-2,-5,-3]

        then

        internal_level_number(0) is 0
        internal_level_number(1) is -2
        internal_level_number(2) is -3
        internal_level_number(3) is -4

        Returns None if the level does not exist.
        """
        reference_levels = sorted(set(self.levels), reverse=True)
        i = abs(i)
        if i >= len(reference_levels):
            return None
        else:
            return reference_levels[i]

    def level_number(self, l):
        r"""
        Return the relative level number (positive!) of l, e.g.

        self.levels = [0,-2,-5,-3]

        then

        level_number(0) is 0
        level_number(-2) is 1
        level_number(-3) is 2
        level_number(-5) is 3

        Returns None if the level does not exist.
        """
        reference_levels = sorted(set(self.levels), reverse=True)
        try:
            return reference_levels.index(l)
        except ValueError:
            return None

    def is_long(self, e):
        r"""
        Check if edge e is long, i.e. passes through more than one level.
        """
        try:
            return self._is_long[e]
        except KeyError:
            il = abs(self.level_number(self.levelofleg(
                e[0])) - self.level_number(self.levelofleg(e[1]))) > 1
            self._is_long[e] = il
            return il

    def has_long_edge(self):
        if self._has_long_edge is None:
            for e in self.edges:
                if self.is_long(e):
                    self._has_long_edge = True
                    break
            else:
                self._has_long_edge = False
        return self._has_long_edge

    def lowest_level(self):
        return self.sortedlevels[0]

    def is_horizontal(self, e=None):
        r"""
        Check if edge e is a horizontal edge or if self has a horizontal edge.
        """
        if e is None:
            return any(self.is_horizontal(e) for e in self.edges)
        if e not in self.edges:
            print("Warning: " + repr(e) + " is not an edge of this graph!")
            return False
        return self.levelofleg(e[0]) == self.levelofleg(e[1])

    def has_loop(self, v):
        r"""
        Check if vertex v has a loop.
        """
        return any(self.vertex(e[0]) == self.vertex(e[1])
                   for e in self.edgesatvertex(v))

    def edgesgoingupfromlevel(self, l):
        r"""
        Return the edges going up from level l.

        This uses that e[0] is not on a lower level than e[1]!
        """
        return [e for e in self.edges if self.levelofleg(e[1]) == l and
                not self.is_horizontal(e)]

    def verticesabove(self, l):
        r"""
        Return list of all vertices above level l.

        If l is None, return all vertices.
        """
        if l is None:
            return list(range(len(self.genera)))
        return [v for v in range(len(self.genera))
                if self.levelofvertex(v) > l]

    def edgesabove(self, l):
        r"""
        Return a list of all edges above level l, i.e. start and end vertex
        have level > l.
        """
        return [e for e in self.edges
                if self.levelofleg(e[0]) > l and self.levelofleg(e[1]) > l]

    def crosseslevel(self, e, l):
        r"""
        Check if e crosses level l (i.e. starts > l and ends <= l)
        """
        return self.levelofleg(e[0]) > l and self.levelofleg(e[1]) <= l

    def edgesgoingpastlevel(self, l):
        r"""
        Return list of edges that go from above level l to below level l.
        """
        return [e for e in self.edges
                if self.levelofleg(e[0]) > l > self.levelofleg(e[1])]

    def canonical_covers(self):
        r"""
        Compute all possible normalised primitive canonical covers of self.

        WARNING: This function is only supposed to work if self is connected,
        but for performance reasons we do not check this!

        Yields:
            (LevelGraph, dict): Yields level graphs of all possible (abelian) covers together with a dictionary legs of cover -> legs of self.

        EXAMPLES ::

            sage: from admcycles.diffstrata.levelgraph import *
            sage: G = LevelGraph.fromPOlist([1,1], [[1],[2,3,4]], [(1,2)],[0,-4,1,3],[0,-1],2)
            sage: [c[0] for c in G.canonical_covers()]
            [LevelGraph([1, 2],[[1, 2], [3, 4, 5, 6]],[(1, 3), (2, 4)],{1: 0, 2: 0, 3: -2, 4: -2, 5: 2, 6: 4},[0, -1]),
            LevelGraph([1, 1, 2],[[1], [2], [3, 4, 5, 6]],[(1, 3), (2, 4)],{1: 0, 2: 0, 3: -2, 4: -2, 5: 2, 6: 4},[0, 0, -1])]
            sage: [c[0] for c in G.canonical_covers() if c[0].is_legal()]
            []

            sage: G = LevelGraph.fromPOlist([1,1,0], [[1], [2,3], [4,5,6,7]], [(1,2), (3,4)], [0,-4,4,-4,4,-3,-1], [0,-1,-2], 2)
            sage: [c[0] for c in G.canonical_covers()]
            [LevelGraph([1, 1, 0],[[1, 2], [3, 4, 5, 6], [7, 8, 9, 10, 11, 12]],[(1, 3), (2, 4), (5, 7), (6, 8)],{1: 0, 2: 0, 3: -2, 4: -2, 5: 2, 6: 2, 7: -2, 8: -2, 9: 2, 10: 2, 11: -2, 12: 0},[0, -1, -2]),
            LevelGraph([1, 1, 1, 0],[[1, 2], [3, 5], [4, 6], [7, 8, 9, 10, 11, 12]],[(1, 3), (2, 4), (5, 7), (6, 8)],{1: 0, 2: 0, 3: -2, 4: -2, 5: 2, 6: 2, 7: -2, 8: -2, 9: 2, 10: 2, 11: -2, 12: 0},[0, -1, -1, -2]),
            LevelGraph([1, 1, 1, 0],[[1], [2], [3, 4, 5, 6], [7, 8, 9, 10, 11, 12]],[(1, 3), (2, 4), (5, 7), (6, 8)],{1: 0, 2: 0, 3: -2, 4: -2, 5: 2, 6: 2, 7: -2, 8: -2, 9: 2, 10: 2, 11: -2, 12: 0},[0, 0, -1, -2]),
            LevelGraph([1, 1, 1, 1, 0],[[1], [2], [3, 5], [4, 6], [7, 8, 9, 10, 11, 12]],[(1, 3), (2, 4), (5, 7), (6, 8)],{1: 0, 2: 0, 3: -2, 4: -2, 5: 2, 6: 2, 7: -2, 8: -2, 9: 2, 10: 2, 11: -2, 12: 0},[0, 0, -1, -1, -2])]
        """
        # We first build the list of all legs and edges upstears and
        # the store the deck transformation and the legdict.
        ldict = {}  # Dictionarie {legs of cover -> legs of self}
        ldict_inv = {}  # Dictionary {legs of self -> [legs of cover]}
        deck = {}
        poleorders = {}

        next_l = 1
        for l in itertools.chain(*self.legs):
            num_pre = gcd(self.poleorders[l], self.k)
            poleord_pre = ZZ((self.k + self.poleorders[l]) / num_pre) - 1
            preimages = list(range(next_l, num_pre + next_l))
            for ll in preimages:
                ldict[ll] = l
                poleorders[ll] = poleord_pre
                deck[ll] = ll + 1 if ll + 1 < num_pre + next_l else next_l
            next_l += num_pre
            ldict_inv[l] = preimages

        # determine ONE possible set of edges
        edges = []
        for e in self.edges:
            one_pre = (ldict_inv[e[0]][0], ldict_inv[e[1]][0])
            edges.append(one_pre)
            next_pre = (deck[one_pre[0]], deck[one_pre[1]])
            while next_pre != one_pre:
                edges.append(next_pre)
                next_pre = (deck[next_pre[0]], deck[next_pre[1]])

        # determine the maximal number of preimages of each vertex
        max_num_pres = [ZZ(gcd([self.poleorders[l] for l in ls] + [self.k])) for ls in self.legs]

        covers = []

        # iterate over all possible combinations of numbers of pre-images
        for num_pres in cartesian_product_iterator([mnp.divisors() for mnp in max_num_pres]):
            # for each vertex, distribute the legs according to the number of preimages
            legs = []
            levels = []
            for v, np in enumerate(num_pres):
                levels.extend([self.levels[v] for _ in range(np)])
                legs_v = [[] for _ in range(np)]
                for l in self.legs[v]:
                    next_l = ldict_inv[l][0]
                    for i in range(len(ldict_inv[l])):
                        legs_v[i % np].append(next_l)
                        next_l = deck[next_l]
                legs.extend(legs_v)
            # now compute the genera
            genera = []
            for ls in legs:
                sum_poles = sum(poleorders[l] for l in ls)
                g = (sum_poles + 2) / 2
                assert g.is_integer()
                genera.append(ZZ(g))

            LG = admcycles.diffstrata.levelgraph.LevelGraph(genera, legs, edges, poleorders, levels, 1, self.k, deck)
            # now we need to iterate over all criss-cross variants
            covers.extend((cc, ldict)
                          for cc in LG.list_all_criss_cross_unique()
                          if cc.is_connected())

        return covers

    def is_legal(self):
        r"""
        Check if self has illegal vertices or edges, assuming that self is a primitive quadratic differential.

        excluded_poles may be a tuple (hashing!) of poles to be excluded for r-GRC.

        WARNING: This function is only supposed to work if self is connected,
        but for performance reasons we do not check this!

        Return boolean
        """
        if self.k == 1:
            return self.embedded_level_graph().is_legal()
        else:
            # Note that if there aren't any covers, we must return false.
            return any(cov.is_legal() for cov, _ in self.canonical_covers())

    def prongs_list(self):
        return list(self.prongs.items())

    def prong(self, e):
        r"""
        The prong order is the pole order of the higher-level pole +k
        """
        if self.levelofleg(e[0]) > self.levelofleg(e[1]):
            return self.orderatleg(e[0]) + self.k
        else:
            return self.orderatleg(e[1]) + self.k

    def automorphisms(self):
        r"""
        Return a list of all automorphisms of self.
        If deck is non-trivial, only those automorphisms that commute with deck
        are listed.
        Each automorphism is given as tuple (vmap, lmap).

        EXAMPLES::

            sage: from admcycles.diffstrata.levelgraph import LevelGraph
            sage: LG = LevelGraph([1, 0], [[1, 2], [3, 4, 5, 6]], [(2, 4), (1, 3)], {1: 0, 2: 0, 3: -4, 4: -4, 5: -1, 6: 5}, [0, -1], 2)
            sage: LG.automorphisms() == \
            ....:   [({1: 1, 0: 0}, {5: 5, 6: 6, 3: 3, 4: 4, 2: 2, 1: 1}), \
            ....:   ({1: 1, 0: 0}, {5: 5, 6: 6, 3: 4, 4: 3, 2: 1, 1: 2})]
            True
        """
        return self.isomorphisms(self)

    def isomorphisms(self, other):
        r"""
        Return a list of all isomorphisms of self and other.
        If deck is non-trivial, only those isomorphisms that commute with deck
        are listed.
        Each isomorphism is given as tuple (vmap, lmap).

        EXAMPLES::

            sage: from admcycles.diffstrata.levelgraph import LevelGraph
            sage: LG = LevelGraph([1, 0], [[1, 2], [3, 4, 5, 6]], [(2, 4), (1, 3)], {1: 0, 2: 0, 3: -4, 4: -4, 5: -1, 6: 5}, [0, -1], 2)
            sage: LG.automorphisms() == LG.isomorphisms(LG)
            True
        """
        def div(G, v):
            r"""
            Return the ordered (!) type of the vertex, i.e. the divisor of the
            differential on the corresponding connected component.
            """
            return sorted(G.poleorders[l] for l in G.legs[v])

        def mapping_orders(p):
            r"""
            Given a list p, yield all possible permutations of p that fix the
            first element.
            """
            for pp in itertools.permutations(p[1:]):
                yield [p[0]] + list(pp)

        def complete_lmap(vs, vmap, part_lmap):
            r"""
            Given a map of the vertices vs of self, returs all possible
            completions of the partial leg map.
            """
            cur_part_maps = [part_lmap]
            for v in vs:
                # We need to map the unmapped legs of v to the unmapped
                # legs of vmap[v], such that the poleorders agree
                # TODO can be imporved by sorting first
                maps_v = []
                one_map = cur_part_maps[0]
                ls_v1 = [l for l in self.legs[v] if l not in one_map]
                ls_v2 = [l for l in other.legs[vmap[v]] if l not in one_map.values()]
                for perm in itertools.permutations(ls_v2):
                    candidate = dict(zip(ls_v1, perm))
                    if any(self.poleorders[l1] != other.poleorders[l2]
                           for l1, l2 in candidate.items()):
                        continue
                    maps_v.append(candidate)
                _cur_part_maps = cur_part_maps
                cur_part_maps = []
                for m, m_v in cartesian_product_iterator([_cur_part_maps, maps_v]):
                    mm = m.copy()
                    mm.update(m_v)
                    cur_part_maps.append(mm)
                if self.cover_of_k > 1:
                    # Make sure that the current transformation is invariant
                    # unde the deck transformations and add all legs that are
                    # no forced.
                    new_legs = ls_v1
                    while new_legs:
                        l1 = new_legs.pop()
                        l1_deck = self.deck[l1]
                        if l1_deck not in new_legs and l1_deck not in cur_part_maps[0]:
                            new_legs.append(l1_deck)
                        _cur_part_maps = cur_part_maps
                        cur_part_maps = []
                        for m in _cur_part_maps:
                            l2 = m[l1]
                            l2_deck = other.deck[l2]
                            if l1_deck in m:
                                if m[l1_deck] == l2_deck:
                                    cur_part_maps.append(m)
                            else:
                                if l2_deck not in m.values():
                                    m[l1_deck] = l2_deck
                                    cur_part_maps.append(m)
                        if not cur_part_maps:
                            return []

            return cur_part_maps

        def level_isomorphisms(l_self, l_other, part_lmap):
            r"""
            An iterator yielding all isomorphisms of the given level that
            respect the partial leg map given my part_lmap.
            """
            vs_self = self.verticesonlevel(l_self)
            vs_other = other.verticesonlevel(l_other)
            """
            # We already checked this
            if len(vs_self) != len(vs_other):
                return
            if sorted(div(self, v) for v in vs_self) != sorted(div(other, v) for v in vs_other):
                return
            """
            vmap = {}
            # Some part of vmap is already fixed by part_lmap
            for l1, l2 in part_lmap.items():
                v1 = self.vertex(l1)
                if v1 in vs_self:
                    if v1 in vmap:
                        if vmap[v1] != other.vertex(l2):
                            return
                    vmap[v1] = other.vertex(l2)
            # Make sure the vertices mapped so far have the same types
            for v1, v2 in vmap.items():
                if div(self, v1) != div(other, v2):
                    return
            # For the remaining vertices, we need to map them to a vertice
            # of the same type.
            vs_unmapped_self = [v for v in vs_self if v not in vmap]
            vs_unmapped_other = [v for v in vs_other if v not in vmap.values()]
            vs_unmapped_by_type_self = defaultdict(list)
            vs_unmapped_by_type_other = defaultdict(list)
            for v in vs_unmapped_self:
                vs_unmapped_by_type_self[tuple(div(self, v))].append(v)
            for v in vs_unmapped_other:
                vs_unmapped_by_type_other[tuple(div(other, v))].append(v)
            # We now iterate over all possible completions of vmap
            part_vmaps = [vmap]
            for mu in vs_unmapped_by_type_self:
                part_vmaps_old = part_vmaps
                part_vmaps = []
                vs_self = vs_unmapped_by_type_self[mu]
                vs_other = vs_unmapped_by_type_other[mu]
                for perm_other in itertools.permutations(vs_other):
                    for vmap_old in part_vmaps_old:
                        vmap_cur = vmap_old[:]
                        for v_self, v_other in zip(vs_self, perm_other):
                            vmap_cur[v_self] = v_other
                        part_vmaps.append(vmap_cur)

            for vmap_cur in part_vmaps:
                for lmap_cur in complete_lmap(vs_self, vmap_cur, part_lmap):
                    yield (vmap_cur, lmap_cur)

        # we do some quick tests first
        if self.k != other.k:
            return []
        if self.cover_of_k != other.cover_of_k:
            return []
        if len(self.genera) != len(other.genera):
            return []
        if len(self.edges) != len(other.edges):
            return []
        if sorted(self.list_markings()) != sorted(other.list_markings()):
            return []

        # The level might have a different numbering
        if not self.numberoflevels() == other.numberoflevels():
            return
        self_levels = sorted(set(self.levels))
        other_levels = sorted(set(other.levels))
        level_dict = dict(zip(other_levels, self_levels))

        # More quick tests
        for l_other, l_self in level_dict.items():
            mus_self = sorted(div(self, v) for v in self.verticesonlevel(l_self))
            mus_other = sorted(div(other, v) for v in other.verticesonlevel(l_other))
            if mus_self != mus_other:
                return []

        vmap = {}
        lmap = {l: l for l in self.list_markings()}
        # Check that lmap preserves the levels
        for l in lmap:
            if self.levels[self.vertex(l)] != level_dict[other.levels[other.vertex(l)]]:
                return []
        # Check that lmap is invariant under deck
        if self.cover_of_k > 1:
            for l in lmap:
                if self.deck[l] != other.deck[l]:
                    return []
        part_maps = [(vmap, lmap)]
        # we travers all levels from bottom to top
        for l_other, l_self in level_dict.items():
            _part_maps = part_maps
            part_maps = []
            es_self = self.edgesgoingupfromlevel(l_self)
            es_other = other.edgesgoingupfromlevel(l_other)
            if len(es_self) != len(es_other):
                return []
            for vmap, lmap in _part_maps:
                for vmap_level, lmap_level in level_isomorphisms(l_self, l_other, lmap):
                    vv = vmap.copy()
                    vv.update(vmap_level)
                    ll = lmap.copy()
                    ll.update(lmap_level)
                    # we add the legs on the higher levels that are now fixed by
                    # the edges
                    good_map = True
                    for e1 in es_self:
                        # by convention, e[1] is on the current level, while
                        # e[0] is on higher level
                        l1_high, l1_cur = e1
                        l2_cur = ll[l1_cur]
                        for e2 in es_other:
                            if e2[1] == l2_cur:
                                l2_high = e2[0]
                                # We need to check that l2_high is actually
                                # on the same level as l1_high
                                if self.levels[self.vertex(l1_high)] != \
                                        level_dict[other.levels[other.vertex(l2_high)]]:
                                    good_map = False
                                    break
                                ll[l1_high] = l2_high
                                break
                        else:
                            raise RuntimeError("No edge found!")
                        if not good_map:
                            break
                    if good_map:
                        part_maps.append((vv, ll))
        # Check that the maps are bijections
        for vmap, lmap in part_maps:
            # assert len(set(vmap.values())) == len(vmap.values()) # map is injective
            assert sorted(vmap.values()) == list(range(len(other.genera)))  # map is surjective
            # assert len(set(lmap.values())) == len(lmap.values()) # map is inkective
            assert sorted(lmap.values()) == sorted(itertools.chain(*other.legs))  # map is surjective
        return part_maps

    def is_connected(self):
        return len(self.connected_component_containing_vertex(0)) == len(self.genera)

    def connected_component_containing_vertex(self, v):
        adjacency_dict = {v: [] for v in range(len(self.genera))}
        for e in self.edges:
            v1 = self.vertex(e[0])
            v2 = self.vertex(e[1])
            adjacency_dict[v1].append(v2)
            adjacency_dict[v2].append(v1)

        # We determine the connected component of the v
        conn_verts = [v]
        to_process = [v]
        while to_process:
            cur = to_process.pop()
            for w in adjacency_dict[cur]:
                if w not in conn_verts:
                    conn_verts.append(w)
                    to_process.append(w)

        return conn_verts

    def _pointtype(self, order):
        if order < 0:
            return "pole"
        elif order == 0:
            return "marked point"
        else:
            return "zero"

    def highestorderpole(self, v):
        r"""
        Returns the leg with the highest order free pole at v, -1 if no poles found.
        """
        minorder = 0
        leg = -1
        for l in self.list_markings(v):
            if self.orderatleg(l) < minorder:
                minorder = self.orderatleg(l)
                leg = l
        return leg

    def rename_legs_by(self, n):
        new_legs = [[l + n for l in ll] for ll in self.legs]
        new_edges = [(e1 + n, e2 + n) for (e1, e2) in self.edges]
        new_poleorders = {l + n: o for l, o in self.poleorders.items()}
        new_deck = {l1 + n: l2 + n for l1, l2 in self.deck.items()}
        return LevelGraph(self.genera, new_legs, new_edges, new_poleorders, self.levels, self.k, self.cover_of_k, new_deck)

    def deck_v(self):
        res = {}
        for v, ls in enumerate(self.legs):
            l = ls[0]
            res[v] = self.vertex(self.deck[l])
        return res

    def set_deck(self, cover_of_k, deck):
        self.cover_of_k = cover_of_k
        self.deck = deck

    def is_isomorphic(self, other):
        """
        Check if self is isomorphic to other.
        If deck is non-trivial, only those isomorphisms that commute with deck
        are considered.
        """
        return bool(self.isomorphisms(other))

    #########################################################
    # end interface
    #########################################################

    def list_all_criss_cross_unique(self):
        import admcycles.diffstrata.auxiliary as aux

        ccs = [self]
        yield self

        # We only need to consider edges for which both ends
        # of the deck-image lie on differnet vertices.
        # TODO there are additional constrains, i.e. there must
        # be more then one edge.
        leg_to_v = {l: v for v, ls in enumerate(self.legs) for l in ls}
        relevant_edges = []
        other_edges = []
        for (l1, l2) in self.edges:
            if leg_to_v[l1] != leg_to_v[self.deck[l1]] and \
                    leg_to_v[l2] != leg_to_v[self.deck[l2]]:
                relevant_edges.append((l1, l2))
            else:
                other_edges.append((l1, l2))

        def graph_from_operation(g):
            new_edges = []
            for e in self.edges:
                if e[0] in g:
                    new_edges.append((g[e[0]], e[1]))
                else:
                    new_edges.append(e)
            return LevelGraph(self.genera, self.legs, new_edges, self.poleorders, self.levels, self.k, self.cover_of_k, self.deck)

        top_ends = [i for i, _ in relevant_edges]
        deck_res = {i: j for i, j in self.deck.items() if i in top_ends}
        top_orbits = aux.get_orbits(deck_res)
        gens = [{i: self.deck[i] for i in o} for o in top_orbits]
        orders = [len(o) for o in top_orbits]
        # We only need to consider those generators of G for which
        # we obtain a non-isomorphic graph by applying them once.
        relevant_gens = []
        relevant_orders = []
        tmp_ccs = []
        for g, o in zip(gens, orders):
            LG = graph_from_operation(g)
            if not self.is_isomorphic(LG):
                relevant_gens.append(g)
                relevant_orders.append(o)
                if not any(G.is_isomorphic(LG) for G in tmp_ccs):
                    tmp_ccs.append(LG)
                    yield LG
        ccs.extend(tmp_ccs)

        # Now we need to compute all the group elements of the form
        # g1^d1 * ... * gn^dn where gi are relevant gens and there is
        # more then on di != 0
        powers = []
        for g, o in zip(relevant_gens, relevant_orders):
            pws = {1: g}
            tmp = g
            for i in range(2, o):
                tmp = {i: g[j] for i, j in tmp.items()}
                pws[i] = tmp
            powers.append(pws)

        group_elems = []
        for ds in cartesian_product_iterator([range(0, o) for o in relevant_orders]):
            if sum(ds) <= 1:
                continue
            tmp = {}
            for d, pws in zip(ds, powers):
                if d == 0:
                    continue
                tmp.update(pws[d])
            group_elems.append(tmp)
        # For those group elements, we create the corresponding graphs
        for g in group_elems:
            LG = graph_from_operation(g)
            if not any(G.is_isomorphic(LG) for G in ccs):
                ccs.append(LG)
                yield LG

    def list_all_criss_cross(self):
        import admcycles.diffstrata.auxiliary as aux

        half_edges = [i for i, _ in self.edges]
        deck_res = {i: j for i, j in self.deck.items() if i in half_edges}
        orbits = aux.get_orbits(deck_res)
        group = [{}]
        for o in orbits:
            deck_o = {i: i for i in o}
            group_old = group
            group = []
            for _ in range(len(o)):
                for g in group_old:
                    g = g.copy()
                    g.update(deck_o)
                    group.append(g)
                deck_o = {i: self.deck[j] for i, j in deck_o.items()}
        for g in group:
            new_edges = [(g[e[0]], e[1]) for e in self.edges]
            yield LevelGraph(self.genera, self.legs, new_edges, self.poleorders, self.levels, self.k, self.cover_of_k, self.deck)

    #################################################################
    # Extract a subgraph
    #################################################################

    def extract(self, vertices, edges):
        r"""
        Extract the subgraph of self (as a LevelGraph) consisting of vertices
        (as list of indices) and edges (as list of edges).

        Returns the levelgraph consisting of vertices, edges and all legs on
        vertices (of self) with their original poleorders and the original
        level structure.

        Note that the deck transformation of the extracted graph may no longer
        be valid.
        """
        newvertices = deepcopy([self.genera[i] for i in vertices])
        newlegs = deepcopy([self.legs[i] for i in vertices])
        newedges = deepcopy(edges)
        newpoleorders = deepcopy({l: self.orderatleg(l)
                                  for l in itertools.chain(*newlegs)})
        newlevels = deepcopy([self.levels[i] for i in vertices])
        leglist = flatten(newlegs)
        newdeck = {l1: l2 for l1, l2 in self.deck.items() if l1 in leglist and l2 in leglist}
        return LevelGraph(newvertices, newlegs, newedges,
                          newpoleorders, newlevels, self.k, self.cover_of_k, newdeck)

    def levelGraph_from_subgraph(self, G):
        r"""
        Returns the LevelGraph associated to a subgraph of underlying_graph
        (with the level structure induced by self)
        """
        vertex_list = [v[0] for v in G.vertices() if v[2] == 'LG']
        return self.extract(vertex_list, G.edge_labels())

    def quotient_with_map(self):
        import admcycles.diffstrata.auxiliary as aux
        ltau = self.deck
        vtau = self.deck_v()
        l_orbits = aux.get_orbits(ltau)  # Orbits are the new legs, numbered 1,...,n
        # We sort the l_orbits to make sure that the new legs are numbered in some deterministic way.
        l_orbits.sort(key=lambda x: min(x))
        v_orbits = aux.get_orbits(vtau)  # Orbits are the new vertices, numbered 0,...,m
        lmap = {}
        for i, o in enumerate(l_orbits):
            for l in o:
                lmap[l] = i + 1
        vmap = {}
        for i, o in enumerate(v_orbits):
            for v in o:
                vmap[v] = i

        leg_attachment = {}  # Dictionary of new legs -> new vertices
        new_poleorders = {}
        for i, o in enumerate(l_orbits):
            leg_attachment[i + 1] = vmap[self.vertex(o[0])]
            new_poleorders[i + 1] = (self.poleorders[o[0]] + 1) * len(o) - self.cover_of_k

        new_legs = [[] for _ in v_orbits]
        for l, v in leg_attachment.items():
            new_legs[v].append(l)

        attached_poles = [[new_poleorders[l] for l in ls] for ls in new_legs]
        new_genera = [int((sum(poles) / self.cover_of_k + 2) / 2) for poles in attached_poles]

        new_edges = list({tuple(sorted([lmap[l1], lmap[l2]])) for l1, l2 in self.edges})
        new_levels = [self.levels[o[0]] for o in v_orbits]

        base = LevelGraph(new_genera, new_legs, new_edges, new_poleorders,
                          new_levels, self.cover_of_k, self.cover_of_k)

        return (base, lmap)

    def quotient(self):
        return self.quotient_with_map()[0]

    def embedded_level_graph(self):
        r"""
        Returns one possible embedded level graph with self as the underlying graph

        WARNING: This function is only supposed to work if self is connected,
        but for performance reasons we do not check this!
        """
        sig = []
        dmp = {}
        deck = {}
        for i, l in enumerate(self.list_markings()):
            sig.append(self.poleorders[l])
            dmp[l] = (0, i)
        for l, p in dmp.items():
            deck[p] = dmp[self.deck[l]]
        dlevels = {l: l for l in self.levels}
        sig_list = [Signature(sig, self.k)]
        X = admcycles.diffstrata.generalisedstratum.GeneralisedStratum(sig_list, self.cover_of_k, deck, canonicalize=True)
        # We need to correct dmp
        dmp = {l: X._rename_dict[p] for l, p in dmp.items()}
        return admcycles.diffstrata.embeddedlevelgraph.EmbeddedLevelGraph(X, self, dmp, dlevels)

    #################################################################
    # Squishing ---- i.e. contracting horizontal edges / levels
    #################################################################

    def squish_horizontal(self, e):
        r"""
        Squish the horizontal edge e and return the new graph.

        Note that deck may no longer be valid afterwards.

        EXAMPLES::

            sage: from admcycles.diffstrata import *
            sage: G = LevelGraph.fromPOlist([1,1], [[1,2],[3,4]], [(2,3)], [1,-1,-1,1],[0,0])
            sage: G.squish_horizontal((2,3))
            LevelGraph([2],[[1, 4]],[],{1: 1, 4: 1},[0])
        """
        # make sure v <= w
        [v, w] = sorted([self.vertex(e[0]), self.vertex(e[1])])

        if not self.is_horizontal(e):
            print("Warning: " + repr(e) +
                  " is not a horizontal edge -- Not contracting.")
            return (self.genera, self.legs, self.edges,
                    self.poleorders, self.levels)

        newgenera = deepcopy(self.genera)
        newlegs = deepcopy(self.legs)
        newedges = deepcopy(self.edges)
        newpoleorders = deepcopy(self.poleorders)
        newlevels = deepcopy(self.levels)
        newdeck = {l1: l2 for l1, l2 in self.deck.items()
                   if l1 not in e and l2 not in e}
        if v == w:
            # --horizontal loop getting contracted---
            # add genus to node:
            newgenera[v] += 1
        else:
            # --horizontal edge getting contracted---
            # transfer genus and legs from w to v:
            newgenera[v] += newgenera[w]
            newlegs[v] += newlegs[w]
            # remove all traces of w
            newgenera.pop(w)
            newlegs.pop(w)
            newlevels.pop(w)
        # remove edge:
        newedges.remove(e)
        # remove legs
        newlegs[v].remove(e[0])
        newlegs[v].remove(e[1])
        # remove poleorders
        del newpoleorders[e[1]]
        del newpoleorders[e[0]]

        return LevelGraph(newgenera, newlegs, newedges, newpoleorders, newlevels, self.k, self.cover_of_k, newdeck)

    def squish_vertical_slow(self, l, adddata, quiet=False):
        r"""
        Squish the level l (and the next lower level!) and return the new graph.
        If addata=True is specified, we additionally return a boolean that tells
        us if a level was squished and the legs that no longer exist.

        More precisely, adddata=True returns a tuple (G,boolean,legs) where
        * G is the (possibly non-contracted) graph and
        * boolean tells us if a level was squished
        * legs is the (possibly empty) list of legs that don't exist anymore

        Implementation:
        At the moment, we remember all the edges (ee) that connect level l to
        level l-1. We then create a new LevelGraph with the same data as self,
        the only difference being that all vertices of level l-1 have now been
        assigned level l. We then remove all (now horizontal!) edges in ee.

        In particular, all points and edges retain their numbering (only the level might have changed).

        WARNING: Level l-1 does not exist anymore afterwards!!!

        Downside: At the moment we get a warning for each edge in ee, as these
        don't have legal pole orders for being horizontal (so checkedgeorders
        complains each time the constructor is called :/).
        """
        if l is None:
            return self
        ll = self.nextlowerlevel(l)
        if ll is False:
            if not quiet:
                print("Warning: Illegal level to contract: " + repr(l))
            if adddata:
                return (self, False, None)
            else:
                return self

        if not quiet:
            print("Squishing levels", l, "and", ll)

        vv = self.verticesonlevel(ll)
        # edges that go to next lower level, i.e. will be contracted
        ee = [e for e in self.edgesatlevel(l)
              if self.levelofleg(e[1]) == ll and not self.is_horizontal(e)]

        if not quiet:
            print("Contracting edges", ee)

        newgenera = deepcopy(self.genera)
        newlegs = deepcopy(self.legs)
        newedges = deepcopy(self.edges)
        newpoleorders = deepcopy(self.poleorders)
        newlevels = deepcopy(self.levels)
        newdeck = deepcopy(self.deck)

        # raise level
        for v in vv:
            newlevels[v] = l
        returngraph = LevelGraph(
            newgenera,
            newlegs,
            newedges,
            newpoleorders,
            newlevels,
            self.k,
            self.cover_of_k,
            newdeck,
            True)
        # contract edges that are now horizontal:
        for e in ee:
            returngraph = returngraph.squish_horizontal(e)

        if adddata:
            return (returngraph, True, flatten(ee))
        else:
            return returngraph

    def squish_vertical(self, l, quiet=True):
        r"""
        Squish the level l (and the next lower level!) and return the new graph.

        WARNING: Level l-1 does not exist anymore afterwards!!!

        Implementation:
        At the moment, we remember all the edges (ee) that connect level l to
        level l-1. We then create a new LevelGraph with the same data as self,
        the only difference being that all vertices of level l-1 have now been
        assigned level l. We then remove all edges in ee.

        (In contrast to the old squish_vertical_slow, this is now done in
        one step and not recursively so we avoid a lot of the copying of graphs.)

        In particular, all points and edges retain their numbering (only the level might have changed).

        Args:
            l (int): (internal) level number
            quiet (bool, optional): No output. Defaults to True.

        Returns:
            LevelGraph: new levelgraph with one level less.

        EXAMPLES::

            sage: from admcycles.diffstrata import *
            sage: G = LevelGraph.fromPOlist([1,2], [[1],[2,3,4]], [(1,2)], [0,-2,1,3],[0,-1])
            sage: G.squish_vertical(0)
            LevelGraph([3],[[3, 4]],[],{3: 1, 4: 3},[0])
        """
        if l is None:
            return self
        ll = self.nextlowerlevel(l)
        levelset = {l, ll}
        if ll is False:
            if not quiet:
                print("Warning: Illegal level to contract: " + repr(l))
            else:
                return LevelGraph(self.genera, self.legs, self.edges,
                                  self.poleorders, self.levels, self.k, self.cover_of_k, self.deck)
        vertices = self.verticesonlevel(l) + self.verticesonlevel(ll)
        # edges that go to next lower level, i.e. will be contracted
        edges = [e for e in self.edges if {self.levelofleg(e[0]), self.levelofleg(e[1])} == levelset]
        # we use two dictionaries for quick lookup while we reorder the info:
        genus_of_vertex = {v: self.genus(v) for v in vertices}
        vertex_of_leg = {
            leg: v for v in vertices for leg in self.legsatvertex(v)}
        legs_of_vertex = {v: self.legsatvertex(v)[:] for v in vertices}
        deleted_legs = []
        while edges:
            start, end = edges.pop()
            v = vertex_of_leg[start]
            w = vertex_of_leg[end]
            del vertex_of_leg[start]
            del vertex_of_leg[end]
            legs_of_vertex[v].remove(start)
            legs_of_vertex[w].remove(end)
            deleted_legs.extend([start, end])
            if v == w:
                # if e (now) connects a vertex to itself, increase its genus
                genus_of_vertex[v] += 1
            else:
                # if e connects two different vertices, combine their data
                # (moving everything from w to v)
                genus_of_vertex[v] += genus_of_vertex[w]
                del genus_of_vertex[w]
                for leg in legs_of_vertex[w]:
                    vertex_of_leg[leg] = v
                    legs_of_vertex[v].append(leg)
                del legs_of_vertex[w]
        # Build new graph with this data:
        # We use sets for more efficient lookup:
        vertices = set(vertices)
        deleted_legs = set(deleted_legs)

        newgenera = []
        newlegs = []
        newlevels = []
        # we copy the untouched vertices and insert the new ones:
        for v in range(len(self.genera)):
            if v in vertices:
                if v in genus_of_vertex:
                    newgenera.append(genus_of_vertex[v])
                    newlegs.append(legs_of_vertex[v][:])
                    newlevels.append(l)
                # otherwise it has been deleted
            else:
                newgenera.append(self.genus(v))
                newlegs.append(self.legsatvertex(v)[:])
                newlevels.append(self.levelofvertex(v))

        # for the edges, we remove those with a deleted edge:
        newedges = []
        for start, end in self.edges:
            if start in deleted_legs:
                assert end in deleted_legs
                continue
            assert end not in deleted_legs
            newedges.append((start, end))
        # for the new poleorders, we only have to remove the deleted legs:
        newpoleorders = {
            leg: p for leg,
            p in self.poleorders.items() if leg not in deleted_legs}

        # we gather the subset of deck consisting of the still existing
        # vertices
        leglist = flatten(newlegs)
        newdeck = {l1: l2 for l1, l2 in self.deck.items() if l1 in leglist}
        assert all(l in leglist for l in newdeck.values())

        return LevelGraph(newgenera, newlegs, newedges, newpoleorders,
                          newlevels, self.k, self.cover_of_k, newdeck)

    def delta(self, k, quiet=False):
        r"""
        Squish all levels except for the k-th.

        Note that delta(1) contracts everything except top-level and that
        the argument is interpreted via internal_level_number

        WARNING: Currently giving an out of range level (e.g.
            0 or >= maxlevel) squishes the entire graph!!!

        Return the corresponding divisor.
        """
        G = self
        if self.is_horizontal():
            raise NotImplementedError("Cannot delta a graph with horizontal edges!")
        # recursive squishing forces us to go backwards, as the lower squished
        # level is always removed.
        for i in reversed(range(self.numberoflevels() - 1)):
            # unfortunately delta and squish use slightly incompatible level
            # numbering, as the "illegal" squish here is k-1
            if abs(k) - 1 == i:
                continue
            if not quiet:
                print("Squishing level", i)
            # note that it is important that internal_level_number of
            # self and not G is called in the argument, as the relative
            # numbering of levels in G changes, but the internal names don't
            G = G.squish_vertical(self.internal_level_number(i), quiet=quiet)
        return G

    #################################################################
    # end Squishing
    #################################################################

    def relabel(self, legdict, tidyup=True):
        new_legs = []
        for vlegs in self.legs:
            list1 = [legdict[i] for i in vlegs]
            if tidyup:
                list1.sort()
            new_legs.append(list1)
        new_edges = [(legdict[e[0]], legdict[e[1]]) for e in self.edges]
        new_poleorders = {legdict[i]: j for i, j in self.poleorders.items()}
        new_deck = {legdict[i]: legdict[j] for i, j in self.deck.items()}
        if tidyup:
            new_edges.sort()
            new_poleorders = dict(sorted(new_poleorders.items()))
        newLG = LevelGraph(self.genera, new_legs, new_edges,
                           new_poleorders, self.levels, self.k, self.cover_of_k,
                           new_deck)
        return newLG

    #################################################################
    # Twist groups
    #################################################################

    def twist_group(self, quiet=True, with_degree=False):
        r"""
        This should not be needed! The stack factor should be computed
        using AdditiveGenerator.stack_factor!!!

        Calculate the index of the simple twist group inside the twist group.

        with_degree=True: return a tuple (e,g) where
            * e = index of simple twist in twist
            * g = index of twist group in level rotation group

        """
        # horizontal edges => trivial twist group :-)
        if self.is_horizontal():
            return 1
        N = len(self.edges)
        M = FreeModule(ZZ, N)
        # kernel of projections to Z/prong Z
        K = M.submodule([M.basis()[i] * self.prong(e)
                         for i, e in enumerate(self.edges)])
        if not quiet:
            print("kernel:", K)
        # submodule generated by edges crossing level i
        # i.e. image of R^Gamma (level rotation group) in ZZ^edges
        t_basis = [sum([M.basis()[i] for i, e in enumerate(self.edges)
                        if self.crosseslevel(e, self.internal_level_number(l))])
                   for l in range(1, self.numberoflevels())]
        E = M.submodule(t_basis)
        if not quiet:
            print("t-module:", E)
        # simple twist group: lcm of delta(l) edge prongs*t_i
        deltas = [self.delta(l, True) for l in range(1, self.numberoflevels())]
        if not quiet:
            print("deltas:", deltas)
        ll = [lcm([d.prong(e) for e in d.edges]) for d in deltas]
        if not quiet:
            print("lcms:", ll)
        st_basis = [sum([ll[l - 1] * M.basis()[i] for i, e in enumerate(self.edges)
                         if self.crosseslevel(e, self.internal_level_number(l))])
                    for l in range(1, self.numberoflevels())]
        S = M.submodule(st_basis)
        if not quiet:
            print("simple twist group:")
            print(S)
            print("Intersection:")
            print(K.intersection(E))
        # K.intersection(E) = Twist group (in ZZ^edges)
        tw_gr = K.intersection(E)
        if with_degree:
            return (S.index_in(tw_gr), tw_gr.index_in(E))
        else:
            return S.index_in(tw_gr)

    #########################################################
    # Checks
    #########################################################
    def checkadmissible(self, quiet=False):
        r"""
        Run all kinds of checks on the level graph. Currently:
         * Check if orders on each komponent add up to k*(2g-2)
         * Check if graph is stable
         * Check if orders at edges sum to -k*2
         * Check if orders respect level crossing
        """
        admissible = True
        if not self.checkorders(quiet):
            if not quiet:
                print("Warning: Illegal orders!")
            admissible = False
        if not self.is_stable(quiet):
            if not quiet:
                print("Warning: Graph not stable!")
            admissible = False
        if not self.checkedgeorders(quiet):
            if not quiet:
                print("Warning: Illegal orders at a node!")
            admissible = False
        return admissible

    def checkorders(self, quiet=False):
        r"""
        Check if the orders add up to k*(2g-2) on each component.
        """
        for v, g in enumerate(self.genera):
            if sum(self.ordersonvertex(v)) != self.sig.k * (2 * g - 2):
                if not quiet:
                    print("Warning: Component " +
                          repr(v) +
                          " orders add up to " +
                          repr(sum(self.ordersonvertex(v))) +
                          " but component is of genus " +
                          repr(g))
                return False
        return True

    def is_stable(self, quiet=False):
        r"""
        Check if graph is stable.
        """
        # total graph:
        e = 2 * self.genus() - 2 + sum(len(legs) for legs in self.legs)
        if e < 0:
            if not quiet:
                print("Warning: Total graph not stable! 2g-2+n = " + repr(e))
            return False

        # components:
        for v in range(len(self.genera)):
            if 3 * self.genus(v) - 3 + len(self.legsatvertex(v)) < 0:
                if not quiet:
                    print("Warning: Component " +
                          repr(v) +
                          " is not stable: g = " +
                          repr(self.genus(v)) +
                          " but only " +
                          repr(len(self.legsatvertex(v))) +
                          " leg(s)!")
                return False
        return True

    def checkedgeorders(self, quiet=False):
        r"""
        Check that the orders at nodes (i.e. edges) sum to -k*2 and that lower
        level has lower order.
        """
        for e in self.edges:
            orders = self.orderatleg(e[0]) + self.orderatleg(e[1])
            if orders != -self.sig.k * 2:
                if not quiet:
                    print(
                        "Warning: Orders at edge " +
                        repr(e) +
                        " add up to " +
                        repr(orders) +
                        " instead of -k*2!")
                return False
            # iff the pole order at e[0] is > the poleorder at e[1], e[0]
            # should be on a higher level than e[1]
            if (sign(self.orderatleg(e[0]) - self.orderatleg(e[1])) !=
                    sign(self.levelofleg(e[0]) - self.levelofleg(e[1]))):
                if not quiet:
                    print("Warning: Orders at edge " + repr(e) +
                          " do not respect level crossing!")
                    print("Poleorders are",
                          (self.orderatleg(e[0]), self.orderatleg(e[1])),
                          "but levels are",
                          (self.levelofleg(e[0]), self.levelofleg(e[1]))
                          )
                return False
        return True

    #################################################################
    # End checks
    #################################################################

    #################################################################
    # Cleanup functions
    # USE ONLY IN __init__!!! LevelGraphs should be static!!!
    #################################################################
    def _init_more(self):
        r"""
        This should be used _only_ for initialisation!

        Make sure everything is in order, in particular:
        * sortedlevels is fine
        * legdict is fine
        * maxleg is fine
        * maxlevel is fine
        """
        # TODO make into cached properties
        self.sortedlevels = sorted(self.levels)
        self.maxleg = max([max(j + [0]) for j in self.legs])
        # maxlevel is named a bit misleading, as it is the LOWEST level
        # (the max of the absolute values...)
        self.maxlevel = max([abs(l) for l in self.levels])

    @property
    def legdict(self):
        return {l: v for v in range(len(self.genera))
                for l in self.legs[v]}

    #################################################################
    # Plotting
    #################################################################
    def plot_obj(self):
        r"""
        Return a sage graphics object generated from a sage graph.

        TODO: Some things that are still ugly:
        * No legs!!
        * currently the vertices have labels (index,genus)
        * loops are vertical not horizontal
        """
        # Create the underlying graph with edge label prong order.
        G = Graph([self.genera,
                   [(self.vertex(e[0]), self.vertex(e[1]), self.prong(e))
                    for e in self.edges]],
                  format='vertices_and_edges', loops=True, multiedges=True)
        # unfortunately, sage insists of displaying the vertex name (index)
        # and not only the label (genus), so we display both...
        G.relabel(list(enumerate(self.genera)))
        h = {l: [(v, self.genus(v)) for v in self.verticesonlevel(l)]
             for l in self.levels}
        # at the moment we just display a list of legs and orders at the bottom
        legtext = "Marked points: "
        for l in sorted(set(self.levels)):
            points_at_level = [leg for leg in self.list_markings()
                               if self.levelofleg(leg) == l]
            if not points_at_level:
                continue
            legtext += "\nAt level %s: " % l
            for leg in points_at_level:
                legtext += "\n" + self._pointtype(leg)
                legtext += " of order " + repr(self.orderatleg(leg))
        print(legtext)
        return G.plot(edge_labels=True, vertex_size=1000,
                      heights=h, layout='ranked') + text(legtext, (0, -0.1),
                                                         vertical_alignment='top', axis_coords=True, axes=False)

    def _unicode_art_(self):
        r"""
        Return unicode art for ``self``.

        """
        from sage.typeset.unicode_art import UnicodeArt
        N = len(self.genera)
        all_half_edges = flatten(self.edges)
        half_edges = {i: [j for j in legs_i if j in all_half_edges]
                      for i, legs_i in zip(range(N), self.legs)}
        open_edges = {i: [j for j in legs_i if j not in all_half_edges]
                      for i, legs_i in zip(range(N), self.legs)}
        left_box = [u' ', u'╭', u'│', u'╰', u' ']
        right_box = [u'  ', u'╮ ', u'│ ', u'╯ ', u'  ']
        positions = {}
        boxes = UnicodeArt()
        total_width = 0
        for vertex in range(N):
            t = list(left_box)
            for v in half_edges[vertex]:
                w = str(self.poleorders[v])
                positions[v] = total_width + len(t[0])
                t[0] += w
                t[1] += u'┴' + u'─' * (len(w) - 1)
            for v in open_edges[vertex]:
                w = str(self.poleorders[v])
                t[4] += w
                t[3] += u'┬' + u'─' * (len(w) - 1)
            t[2] += str(self.genera[vertex]) + " (" + str(self.levelofvertex(vertex)) + ")"
            length = max(len(line) for line in t)
            for i in [0, 2, 4]:
                t[i] += u' ' * (length - len(t[i]))
            for i in [1, 3]:
                t[i] += u'─' * (length - len(t[i]))
            for i in range(5):
                t[i] += right_box[i]
            total_width += length + 2
            boxes += UnicodeArt(t)
        num_edges = len(self.edges)
        matchings = [[u' '] * (1 + max(positions.values()))
                     for _ in range(num_edges)]
        for i, (a, b) in enumerate(self.edges):
            xa = positions[a]
            xb = positions[b]
            if xa > xb:
                xa, xb = xb, xa
            for j in range(xa + 1, xb):
                matchings[i][j] = u'─'
            matchings[i][xa] = u'╭'
            matchings[i][xb] = u'╮'
            for j in range(i + 1, num_edges):
                matchings[j][xa] = u'│'
                matchings[j][xb] = u'│'
        matchings = [u''.join(line) for line in matchings]
        return UnicodeArt(matchings + list(boxes))
