import itertools
from collections import defaultdict

# pylint does not know sage
from sage.graphs.graph import Graph  # pylint: disable=import-error
# sage documentation says compositions are better than OrderedPartitions,
# no idea why....
from sage.combinat.partition import Partitions, OrderedPartitions  # pylint: disable=import-error
from sage.combinat.set_partition import SetPartitions  # pylint: disable=import-error
from sage.combinat.permutation import Permutations  # pylint: disable=import-error
from sage.misc.mrange import cartesian_product_iterator  # pylin: disable=import-error

import admcycles.diffstrata.levelgraph
from admcycles.diffstrata.sig import Signature

############################################################
############################################################
# Old BIC generation:
# This was the first attempt at generating BICs
# (generating all stable graphs with admcycles and then
# putting all possible bipartite structures etc on them).
# It's nice for historic reasons but painfully slow!
# Definitely use bic_alt below instead!!!
############################################################
############################################################


def SageGraph(gr):
    # converts stgraph gr in Sage graph
    legdic = {i: vnum for vnum in range(
        len(gr.legs(copy=False))) for i in gr.legs(vnum, copy=False)}
    return Graph([(legdic[a], legdic[b])
                  for (a, b) in gr.edges(copy=False)], loops=True, multiedges=True)


def bics(g, orders):
    r"""
    Generate BICs for stratum with signature orders of genus g.

    DO NOT USE!!! USE bic_alt instead!!!

    Args:
        g (int): genus
        orders (tuple): signature tuple

    Returns:
        list: list of BICs
    """
    print('WARNING! This is not the normal BIC generation algorithm!')
    print("Only use if you know what you're doing!")
    n = len(orders)
    bound = min(g + n, 3 * g - 3 + n)
    result = []  # list of levelgraphs
    orderdict = {i + 1: orders[i] for i in range(n)}

    for e in range(1, bound + 1):
        # look at which stgraphs in Mbar_g,n with e edges are bipartite
        for gr in new_list_strata(g, n, e):
            check = SageGraph(gr).is_bipartite(True)
            if check[0]:
                # check[1] contains dictionary {vertices: 0 or 1}
                vert1 = [v for v in check[1] if check[1][v] == 0]
                vert2 = [v for v in check[1] if check[1][v] == 1]
                result += bics_on_bipgr(gr, vert1, vert2, orderdict)
                result += bics_on_bipgr(gr, vert2, vert1, orderdict)
    return result


def bics_on_bipgr(gr, vertup, vertdown, orderdict):
    # takes stable graph and partition of range(len(gr.genera)) into vertup, vertdown
    # creates list of possible pole orders at edges
    result = []

    # removed from admcycles so reinserted here for legacy purposes:
    def dicunion(*dicts):
        return dict(itertools.chain.from_iterable(dct.items()
                                                  for dct in dicts))

    numvert = len(gr.genera(copy=False))
    halfedges = gr.halfedges()
    halfedgeinvers = dict(gr.edges(copy=False))
    halfedgeinvers.update({e1: e0 for (e0, e1) in gr.edges(copy=False)})

    levels = [0 for i in range(numvert)]
    for i in vertdown:
        levels[i] = -1

    helist = [[l for l in ll if l in halfedges]
              for ll in gr.legs(copy=False)]  # list of half-edges of each vertex
    # list (with len numvert) of orders that need to be provided by pole
    # orders of half-edges (not legs)
    degs = [2 * gr.genera(v) - 2 - sum([orderdict[l]
                                        for l in gr.list_markings(v)])
            for v in range(numvert)]
    # if degs < 0:
    #     return []

    weightparts = []
    for v in vertup:
        vweightpart = []
        for part in OrderedPartitions(degs[v] + len(helist[v]), len(helist[v])):
            # dictionary of weights from edges connected to vertex v
            vdic = {helist[v][i]: part[i] - 1 for i in range(len(helist[v]))}
            vdic.update(
                {halfedgeinvers[helist[v][i]]: -part[i] - 1 for i in range(len(helist[v]))})
            # print vdic
            vweightpart.append(vdic)
        weightparts += [vweightpart]
        # print weightparts

    for parts in itertools.product(*weightparts):
        # print parts
        poleorders = dicunion(orderdict, *parts)
        CandGraph = admcycles.diffstrata.levelgraph.LevelGraph(
            gr.genera(
                copy=False), gr.legs(
                copy=False), gr.edges(
                copy=False), poleorders, levels)
        if CandGraph.is_legal() and CandGraph.checkadmissible(True):
            result.append(CandGraph)
    return result


def new_list_strata(g, n, r):
    if r < 0 or r > 3 * g - 3 + n:
        raise ValueError('r out of range')
    return [lis[0] for lis in admcycles.admcycles.degeneration_graph(int(g), n, r)[
        0][r]]

############################################################
############################################################

############################################################
############################################################
# New BIC generation. This is currently used.
############################################################
############################################################


def MultisetPartitions(S, k=None):
    r"""
    Return all partitions of the multiset S.
    If k is given, return only partitions of length k.

    EXAMPLES::

        sage: from admcycles.diffstrata.bic import MultisetPartitions
        sage: sorted(MultisetPartitions([1, 1]))
        [((1,), (1,)), ((1, 1),)]

        sage: list(MultisetPartitions([1, 1], 2))
        [((1,), (1,))]

        sage: sorted(MultisetPartitions([1, 1, 2], 2))
        [((1,), (1, 2)), ((1, 1), (2,))]

        sage: list(MultisetPartitions([1], 2))
        []
    """
    if k is not None and len(S) < k:
        return

    seen = set()
    for int_part in SetPartitions(list(range(len(S))), k):
        part = tuple(sorted(tuple(sorted(S[i] for i in p)) for p in int_part))
        if part in seen:
            continue
        seen.add(part)
        yield part


def OrderedMultisetPartitions(S, k):
    r"""
    Return all partitions of the multiset S of length k.

    EXAMPLES::

        sage: from admcycles.diffstrata.bic import OrderedMultisetPartitions
        sage: list(OrderedMultisetPartitions([1, 1], 2))
        [((1,), (1,))]

        sage: sorted(OrderedMultisetPartitions([1, 1, 2], 2))
        [((1,), (1, 2)), ((1, 1), (2,)), ((1, 2), (1,)), ((2,), (1, 1))]
    """
    seen = set()
    for part in MultisetPartitions(S, k):
        for perm in Permutations(part):
            perm = tuple(perm)
            if perm in seen:
                continue
            seen.add(perm)
            yield perm


def OrderedMultisetPartitionsWithEmpty(S, k):
    r"""
    Return all partitions of S in k sets, including partitions
    where the sets are empty.

    EXAMPLES::

        sage: from admcycles.diffstrata.bic import OrderedMultisetPartitionsWithEmpty
        sage: list(OrderedMultisetPartitionsWithEmpty([], 2))
        [((), ())]

        sage: sorted(OrderedMultisetPartitionsWithEmpty([1, 2], 2))
        [((), (1, 2)), ((1,), (2,)), ((1, 2), ()), ((2,), (1,))]

        sage: sorted(OrderedMultisetPartitionsWithEmpty([1, 1], 2))
        [((), (1, 1)), ((1,), (1,)), ((1, 1), ())]
    """

    if not S:
        yield tuple(() for _ in range(k))
        return

    seen = set()

    for kk in range(1, min(k, len(S)) + 1):
        for part in MultisetPartitions(S, kk):
            # We iterate over all permuations (a_0, ..., a_kk) of length kk of
            # the numbers 0..k-1.
            # At position a_i write the element i of the current partition,
            # all other positions are filled with empty tuples.
            for perm in Permutations(range(k), kk):
                perm_dict = {j: i for i, j in enumerate(perm)}
                res = tuple(part[perm_dict[i]] if i in perm_dict else ()
                            for i in range(k))
                if res in seen:
                    continue
                seen.add(res)
                yield tuple(res)


def possible_bottom_levels(k, g_bottom, marked_points_bottom):
    r"""
    Yields all possible bottom levels as a tuple of lists
    (genera, marked_points, sum_orders_edges).

    EXAMPLES::

        sage: from admcycles.diffstrata.bic import possible_bottom_levels
        sage: list(possible_bottom_levels(1, 0, [0, 0, 0, 0]))
        [((0,), ((0, 0, 0, 0),), (-2,)), ((0, 0), ((0, 0), (0, 0)), (-2, -2))]

        sage: list(possible_bottom_levels(1, 2, [2, 2]))
        [((2,), ((2, 2),), (-2,)), ((1, 1), ((2,), (2,)), (-2, -2))]

        sage: list(possible_bottom_levels(1, 1, [0, 2, 2]))
        [((1,), ((0, 2, 2),), (-4,)),
         ((1, 0), ((0, 2), (2,)), (-2, -4)),
         ((1, 0), ((2,), (0, 2)), (-2, -4))]

        sage: list(possible_bottom_levels(1, 2, []))
        []

        sage: list(possible_bottom_levels(2, 2, [2, 2]))
        []

        sage: list(possible_bottom_levels(1, 0, [0]))
        []

        sage: list(possible_bottom_levels(1, 0, []))
        []
    """
    # Catch a pathological case early
    if g_bottom == 0 and not marked_points_bottom:
        return

    # For faster computation, we cache the set partitions of all
    # marked points of length up to g_bottom + 1.
    # More precisely, we cache only those where the sum of the entries in
    # each of the first g_bottom subsets are at least k+1 and the last
    # subset is allowed to be the empty set.

    if g_bottom == 0:
        parts = [[[marked_points_bottom]]]
    else:
        parts = [[] for _ in range(g_bottom + 1)]
        for l in range(1, g_bottom + 1):
            for part in OrderedMultisetPartitions(marked_points_bottom, l):
                if any(sum(p) < k + 1 for p in part):
                    continue
                parts[l].append(list(part) + [[]])
            for part in OrderedMultisetPartitions(marked_points_bottom, l + 1):
                if any(sum(p) < k + 1 for p in part[:-1]):
                    continue
                parts[l].append(list(part))

    # Now replace the last subset with a list of possible distributions
    # to P1's.
    # If the sum of the values on a P1 is < 2k there need to be at
    # least two marked
    # points (in this case there must be a single edge downwards),
    # otherwise there need to be at least one marked point.
    parts2 = [[] for _ in range(len(parts))]
    for l in range(len(parts)):
        for part in parts[l]:
            if not part[-1]:
                # All legs are already distributed to the vertices
                # with g >= 1.
                parts2[l].append(part)
                continue
            for Ppart in MultisetPartitions(part[-1]):
                PpartSum = [sum(p) for p in Ppart]
                if any(s < 1 - k for s in PpartSum):
                    continue
                if any(s < 2 and len(p) == 1
                       for s, p in zip(PpartSum, Ppart)):
                    continue
                parts2[l].append(part[:-1] + [Ppart])

    seen = set()

    def canonicalize(genera, marked_points):
        assert len(genera) == len(marked_points)
        # The genera should already be sorted in descending order. Check it!
        assert all(genera[i] >= genera[i + 1] for i in range(len(genera) - 1))
        marked_points = [tuple(sorted(m)) for m in marked_points]
        while True:
            changed = False
            for i in range(len(genera) - 1):
                if genera[i] != genera[i + 1]:
                    continue
                if marked_points[i] > marked_points[i + 1]:
                    tmp = marked_points[i]
                    marked_points[i] = marked_points[i + 1]
                    marked_points[i + 1] = tmp
                    changed = True
            if not changed:
                return (tuple(genera), tuple(marked_points))

    for genPart in Partitions(g_bottom):
        genPart = sorted(genPart)
        genPart.reverse()  # Make the order descinding
        for part in parts2[len(genPart)]:
            num_rational_comps = len(part[-1])
            genera = genPart + [0 for _ in range(num_rational_comps)]
            marked_points = part[:-1] + list(part[-1])
            # The edges add at least one pole of order -k-1. We discard the current combination if the
            # sum of the current orders is already to low.
            sum_ords = [sum(ms) for ms in marked_points]
            if any(k * (2 * g - 2) > s - k - 1 for g, s in zip(genera, sum_ords)):
                continue
            res = canonicalize(genera, marked_points)
            if res not in seen:
                seen.add(res)
                sum_edge_orders = tuple(k * (2 * g - 2) - s for g, s in zip(genera, sum_ords))
                yield (res[0], res[1], sum_edge_orders)


def possible_edges(k, edge_orders_bottom):
    r"""
    For a given tuple of edge orders at the bottom level (as returned
    as the third value by possible_bottom_levels) yield all
    possible combination of edges, given by a tuple of tuple of
    k-enhancements.

    EXAMPLES::

        sage: from admcycles.diffstrata.bic import possible_edges
        sage: list(possible_edges(1, (-2,)))
        [((1,),)]

        sage: list(possible_edges(1, (-2, -2)))
        [((1,), (1,))]

        sage: list(possible_edges(2, (-5,)))
        [((3,),)]

        sage: list(possible_edges(2, (0,)))
        [((),)]
    """
    for part in cartesian_product_iterator([Partitions(-o, min_part=k + 1) for o in edge_orders_bottom]):
        # Part is a tuple of tuples of the negativ of the orders of the edges at bottom level.
        # We want a tuple of tuples of k-enhancements.
        yield tuple(tuple(abs(-o + k) for o in p) for p in part)

    # TODO: is there a good way to remove duplicates here?
    # (Duplicates may only appear if the bottom level has multiple "similar"
    # vertices.)


def possible_top_levels(k, g_top, num_verts, marked_points_top, ord_edges):
    r"""
    Yields all possible top levels as a tuple of lists
    (genera, marked_points, edges),
    where edges is given as list of indices is ord_edges.

    EXAMPLES::

        sage: from admcycles.diffstrata.bic import possible_top_levels
        sage: list(possible_top_levels(1, 2, 2, (), ((0, 0),)))
        [((1, 1), ((), ()), (((0, 0),), ((0, 1),)))]

        sage: list(possible_top_levels(1, 2, 2, (), ((0,), (0,),)))
        [((1, 1), ((), ()), (((0, 0),), ((1, 0),)))]

        sage: list(possible_top_levels(1, 3, 2, (2,), ((0, 0),)))
        [((2, 1), ((2,), ()), (((0, 0),), ((0, 1),))),
         ((1, 2), ((), (2,)), (((0, 0),), ((0, 1),)))]

        sage: list(possible_top_levels(1, 0, 2, (-4,), ((0, 0),)))
        []

        sage: list(possible_top_levels(2, 3, 2, (2,), ((2, 0),)))
        [((2, 1), ((2,), ()), (((0, 0),), ((0, 1),)))]
    """
    # Return early if there is no solution
    if g_top != (sum(marked_points_top) + sum(e for es in ord_edges for e in es)) / (k * 2) + num_verts:
        return

    # Each component needs at least one edge
    edge_indices = [(v_bot, i) for v_bot in range(len(ord_edges))
                    for i in range(len(ord_edges[v_bot]))]
    for edges in SetPartitions(edge_indices, num_verts):
        # Now distribute the marked points
        for marked_points in OrderedMultisetPartitionsWithEmpty(marked_points_top, num_verts):
            # If the sum at each vertex is not divisible by 2k, discard the current partition
            if any((sum(ms) + sum(ord_edges[v_bot][e] for v_bot, e in es)) % (2 * k) != 0
                    for ms, es in zip(marked_points, edges)):
                continue
            # The partition look valid, calculate the genera
            genera = tuple((sum(ms) + sum(ord_edges[v_bot][e] for v_bot, e in es)) / (2 * k) + 1
                           for ms, es in zip(marked_points, edges))
            # Check that all genera are >= 0
            if any(g < 0 for g in genera):
                continue
            # Check that all rational components are stable
            if any(g == 0 and len(es) + len(ms) < 3 for g, es, ms in zip(genera, edges, marked_points)):
                continue
            edges = tuple(tuple(es) for es in edges)
            yield (genera, marked_points, edges)


def construct_graphs(sig, bottom_level, edges, top_level):
    # The genera
    genera = top_level[0] + bottom_level[0]
    levels = [0 for _ in top_level[0]] + [-1 for _ in bottom_level[0]]

    # We first number the edges comming from marked points
    marked_point_orders = top_level[1] + bottom_level[1]
    legs = [[] for _ in genera]
    poleorders = {}
    l = 1
    for v, mpo in enumerate(marked_point_orders):
        for o in mpo:
            legs[v].append(l)
            poleorders[l] = o
            l += 1

    # Now we handle the edges.
    # Recall that top_level[2] is a list of list with entries of the form (v_bot, i),
    # refering the the i-th edge for the vertes v_bot, i.e. the entry edges[v_bot][i].
    num_vertices_top_level = len(top_level[0])
    edges_graph = []
    for v_top, es in enumerate(top_level[2]):
        for v_bot, e in es:
            legs[v_top].append(l)
            legs[v_bot + num_vertices_top_level].append(l + 1)
            enh = edges[v_bot][e]
            poleorders[l] = enh - sig.k
            poleorders[l + 1] = -enh - sig.k
            # Some parts of the code expect the first index of the edge
            # to be the leg on lower level.
            # TODO: either improve the code such that the order does not matter
            # or at least correctly document and test it!!!
            edges_graph.append((l, l + 1))
            l += 2

    # Up to now, we have completely ignored the order of the marked points.
    # We need to relabel them to match all possible orders of the stratum.
    ord_to_sig_index = defaultdict(list)
    for i, o in enumerate(sig.sig):
        ord_to_sig_index[o].append(i + 1)
    ord_to_leg = defaultdict(list)
    for i in range(1, len(sig.sig) + 1):
        o = poleorders[i]
        ord_to_leg[o].append(i)
    assert sorted(ord_to_leg.keys()) == sorted(ord_to_sig_index.keys())
    orders = ord_to_leg.keys()
    # We iterate over the cartesian product of all permutations of the numers (0, ..., o_i-1)
    # where o_i is the multiplicity the order o appears in the signature.
    first_permutation = True
    # A lot of permutations give isomorphic graph. TODO maybe figure this out befor creating all the graphs?
    found = []
    for mapping_perm in cartesian_product_iterator([
            list(Permutations(list(range(len(ord_to_leg[o]))))) for o in orders]):
        # We build a dict "mapping", such that mapping[l] is the index (counted form 1!)
        # in the signature the leg l should be mapped to.
        mapping = {}
        for o, perm in zip(orders, mapping_perm):
            for i, l in enumerate(ord_to_leg[o]):
                mapping[l] = ord_to_sig_index[o][perm[i]]
        legs_perm = [[mapping[l] if l in mapping else l for l in ls] for ls in legs]
        poleorders_perm = {}
        for l in poleorders:
            if l in mapping:
                poleorders_perm[mapping[l]] = poleorders[l]
            else:
                poleorders_perm[l] = poleorders[l]
        for l in range(1, len(sig.sig) + 1):
            assert poleorders_perm[l] == sig.sig[l - 1]

        graph = admcycles.diffstrata.levelgraph.LevelGraph(genera, legs_perm, edges_graph, poleorders_perm, levels, sig.k)
        if first_permutation:
            if not graph.is_legal():
                return
            first_permutation = False
        if not any(gr.is_isomorphic(graph) for gr in found):
            found.append(graph)
            yield graph


def bic_alt_noiso(sig):
    r"""
    Construct all non-horizontal divisors in the stratum sig.

    More precisely, each BIC is LevelGraph with two levels numbered 0, -1
    and marked points 1,...,n where the i-th point corresponds to the element
    i-1 of the signature.

    Note that this is the method called by GeneralisedStratum.gen_bic.

    Args:
        sig (Signature): Signature of the stratum.

    Returns:
        list: list of 2-level non-horizontal LevelGraphs.

    EXAMPLES::

        sage: from admcycles.diffstrata import *
        sage: assert comp_list(bic_alt(Signature((1,1))),\
        [LevelGraph([1, 0],[[3, 4], [1, 2, 5, 6]],[(3, 5), (4, 6)],{1: 1, 2: 1, 3: 0, 4: 0, 5: -2, 6: -2},[0, -1],True),\
        LevelGraph([1, 1, 0],[[4], [3], [1, 2, 5, 6]],[(3, 5), (4, 6)],{1: 1, 2: 1, 3: 0, 4: 0, 5: -2, 6: -2},[0, 0, -1],True),\
        LevelGraph([1, 1],[[3], [1, 2, 4]],[(3, 4)],{1: 1, 2: 1, 3: 0, 4: -2},[0, -1],True),\
        LevelGraph([2, 0],[[3], [1, 2, 4]],[(3, 4)],{1: 1, 2: 1, 3: 2, 4: -4},[0, -1],True)])

        sage: assert comp_list(bic_alt(Signature((2,))),\
        [LevelGraph([1, 1],[[2], [1, 3]],[(2, 3)],{1: 2, 2: 0, 3: -2},[0, -1],True),\
        LevelGraph([1, 0],[[2, 3], [1, 4, 5]],[(2, 4), (3, 5)],{1: 2, 2: 0, 3: 0, 4: -2, 5: -2},[0, -1],True)])

        sage: assert comp_list(bic_alt(Signature((4,))),\
        [LevelGraph([1, 1, 0],[[2, 4], [3], [1, 5, 6, 7]],[(2, 5), (3, 6), (4, 7)],{1: 4, 2: 0, 3: 0, 4: 0, 5: -2, 6: -2, 7: -2},[0, 0, -1],True),\
        LevelGraph([1, 1, 1],[[3], [2], [1, 4, 5]],[(2, 4), (3, 5)],{1: 4, 2: 0, 3: 0, 4: -2, 5: -2},[0, 0, -1],True),\
        LevelGraph([2, 0],[[2, 3], [1, 4, 5]],[(2, 4), (3, 5)],{1: 4, 2: 2, 3: 0, 4: -4, 5: -2},[0, -1],True),\
        LevelGraph([2, 0],[[2, 3], [1, 4, 5]],[(2, 4), (3, 5)],{1: 4, 2: 1, 3: 1, 4: -3, 5: -3},[0, -1],True),\
        LevelGraph([1, 0],[[2, 3, 4], [1, 5, 6, 7]],[(2, 5), (3, 6), (4, 7)],{1: 4, 2: 0, 3: 0, 4: 0, 5: -2, 6: -2, 7: -2},[0, -1],True),\
        LevelGraph([1, 2],[[2], [1, 3]],[(2, 3)],{1: 4, 2: 0, 3: -2},[0, -1],True),\
        LevelGraph([2, 1],[[2], [1, 3]],[(2, 3)],{1: 4, 2: 2, 3: -4},[0, -1],True),\
        LevelGraph([1, 1],[[2, 3], [1, 4, 5]],[(2, 4), (3, 5)],{1: 4, 2: 0, 3: 0, 4: -2, 5: -2},[0, -1],True)])

        sage: len(bic_alt(Signature((1,1,1,1)))) # long time (2 seconds)
        102

        sage: len(bic_alt(Signature((2,2,0,-2))))
        61

        sage: len(bic_alt((2,2,0,-2)))
        61
    """
    if isinstance(sig, tuple):
        sig = Signature(sig)

    if sig.k == 1:
        # As every g=0 top component needs at least one pole, the bottom max
        # depends on this:
        if sig.p > 0:
            g_bot_max = sig.g
            g_top_min = 0
        else:
            g_bot_max = sig.g - 1
            g_top_min = 1
    else:
        # For k>1-differentials, g=0 top components without poles are
        # possible.
        g_bot_max = sig.g
        g_top_min = 0

    found = set()

    for g_bot, (marked_points_bottom, marked_points_top) in cartesian_product_iterator([
            list(range(g_bot_max + 1)),
            list(OrderedMultisetPartitionsWithEmpty(sig.sig, 2))]):
        for bottom_level in possible_bottom_levels(sig.k, g_bot, marked_points_bottom):
            for edges in possible_edges(sig.k, bottom_level[2]):
                # Check that rational components on bottom level are stable
                if any(g == 0 and len(ms) + len(es) < 3
                        for g, ms, es in zip(bottom_level[0], bottom_level[1], edges)):
                    continue
                num_verts_bottom = len(bottom_level[0])
                num_edges = sum(len(es) for es in edges)
                ord_edges_top = [[e - sig.k for e in es] for es in edges]
                for num_verts_top in range(1, num_edges + 1):
                    num_verts = num_verts_top + num_verts_bottom
                    h_0 = num_edges - num_verts + 1
                    g_top = sig.g - g_bot - h_0
                    if g_top < g_top_min:
                        continue
                    for top_level in possible_top_levels(sig.k, g_top, num_verts_top, marked_points_top, ord_edges_top):
                        # Check that the graph is connected.
                        edges_top = [[] for _ in range(num_verts_top)]
                        edges_bottom = [[] for _ in range(num_verts_bottom)]
                        e = 1
                        for v_top, es in enumerate(top_level[2]):
                            for v_bot, _ in es:
                                edges_bottom[v_bot].append(e)
                                edges_top[v_top].append(e)
                                e += 1
                        if not is_connected(edges_top, edges_bottom):
                            continue

                        for graph in construct_graphs(sig, bottom_level, edges, top_level):
                            if graph not in found:
                                found.add(graph)
                                yield graph


def bic_alt(sig):
    r"""
    The BICs of the stratum sig up to isomorphism of LevelGraphs.

    This should not be used directly, use a GeneralisedStratum or Stratum
    instead to obtain EmbeddedLevelGraphs and the correct isomorphism classes.

    Args:
        sig (tuple): signature tuple

    Returns:
        list: list of LevelGraphs.
    """
    return isom_rep(bic_alt_noiso(sig))  # remove duplicates up to iso


def bic_covers(sig):
    r"""
    Return the list of all primitive canonical covers of BICs of the given
    Signature.
    """
    return [cov
            for graph in bic_alt(sig)
            for cov in graph.canonical_covers()]


def isom_rep(L):
    r"""
    Return a list of representatives of isomorphism classes of L.

    TODO: optimise!
    """
    dist_list = []
    for g in L:
        if all(not g.is_isomorphic(h) for h in dist_list):
            dist_list.append(g)
    return dist_list


def comp_list(L, H):
    r"""
    Compare two lists of LevelGraphs (up to isomorphism).

    Returns a tuple: (list L without H, list H without L)
    """
    return ([g for g in L if not any(g.is_isomorphic(h) for h in H)],
            [g for g in H if not any(g.is_isomorphic(h) for h in L)])


def is_connected(half_edges_top, half_edges_bottom):
    r"""
    Check if graph given by the two sets of half edges is connected.
    We do this by depth-first search.
    Note that we assume that both ends of each edge have the same number,
    i.e. each edge is of the form (j,j).

    EXAMPLES::

        sage: from admcycles.diffstrata.bic import is_connected
        sage: is_connected([[1],[2]],[[1,2]])
        True
        sage: is_connected([[1],[2]],[[2],[1]])
        False
    """
    def _connected_comp(current_edges, other_edges,
                        seen_current, seen_other, current_vert=0):
        seen_current.append(current_vert)
        for e in current_edges[current_vert]:
            for (i, edges) in enumerate(other_edges):
                if e in edges:
                    if i not in seen_other:
                        _connected_comp(other_edges, current_edges,
                                        seen_other, seen_current, i)
                    break
        return (seen_current, seen_other)
    seen_top = []
    seen_bottom = []
    _connected_comp(half_edges_top, half_edges_bottom, seen_top, seen_bottom)
    return len(seen_top) == len(half_edges_top) and len(
        seen_bottom) == len(half_edges_bottom)


def test_bic_algs(sig_list=None):
    r"""
    Compare output of bics and bic_alt.

    EXAMPLES::

        sage: from admcycles.diffstrata import *
        sage: test_bic_algs()  # long time (45 seconds)  # skip, not really needed + long # doctest: +SKIP
        (1, 1): ([], [])
        (1, 1, 0, 0, -2): ([], [])
        (2, 0, -2): ([], [])
        (1, 0, 0, 1): ([], [])
        (1, -2, 2, 1): ([], [])
        (2, 2): ([], [])

        sage: test_bic_algs([(0,0),(2,1,1)])  # long time (50 seconds)  # doctest: +SKIP
        (0, 0): ([], [])
        (2, 1, 1): ([], [])

        sage: test_bic_algs([(1,0,-1),(2,),(4,),(1,-1)])
        WARNING! This is not the normal BIC generation algorithm!
        Only use if you know what you're doing!
        (1, 0, -1): ([], [])
        WARNING! This is not the normal BIC generation algorithm!
        Only use if you know what you're doing!
        (2,): ([], [])
        WARNING! This is not the normal BIC generation algorithm!
        Only use if you know what you're doing!
        (4,): ([], [])
        WARNING! This is not the normal BIC generation algorithm!
        Only use if you know what you're doing!
        (1, -1): ([], [])
    """
    if sig_list is None:
        sig_list = [(1, 1), (1, 1, 0, 0, -2), (2, 0, -2),
                    (1, 0, 0, 1), (1, -2, 2, 1), (2, 2)]
    for sig in sig_list:
        Sig = Signature(sig)
        print(
            "%r: %r" %
            (Sig.sig,
             comp_list(
                 bics(
                     Sig.g,
                     Sig.sig),
                 bic_alt(Sig))))
